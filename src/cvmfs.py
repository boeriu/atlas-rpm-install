import contextlib
from exc import CVMFSTransactionAlreadyOpen, CVMFSPublishError, ShutDown
import log
import monitor
import os
from util import shellCmd, createFile


CVMFS_CMD = '/usr/bin/cvmfs_server'

@contextlib.contextmanager
def transaction(repo):
    _start_cvmfs_transaction(repo)
    try:
        yield
        do_publish = True
    except (ShutDown, Exception, KeyboardInterrupt, SystemExit) as e:
        do_publish = False
        _stop_cvmfs_transaction(repo, e)
    finally:
        if do_publish:
            _run_publish(repo)


def publish(repo):
    log.info('Running cvmfs_server publish:')
    cmd = _end_transaction_cmd(repo)
    result = shellCmd(cmd, asText=True)
    if not result.ok or result.err or result.raised:
        log.error('Failed to publish CVMFS changes')
        log.error('Got exitcode {0}, and output:'.format(result.exitcode))
        raise CVMFSPublishError('Failed to publish CVMFS changes')
    else:
        log.info('Successfully published CVMFS changes')
        monitor.timings.publish(result.runtime)

    out = result.out
    err = result.err
    if out:
        log.info(out)

    if err:
        log.error(err)


def createNestedCatalogs(*dirs):
    for d in dirs:
        if os.path.exists(d):
            path = os.path.join(d, '.cvmfscatalog')
            createFile(path)


def _start_cvmfs_transaction(repo):
    log.info('Opening CVMFS transaction')
    cmd = _start_transaction_cmd(repo)
    result = shellCmd(cmd)
    if not result.ok:
        m = 'A CVMFS transaction is already ongoing'
        log.fatal(m)
        monitor.process.error('Another CVMFS transaction already ongoing')
        raise CVMFSTransactionAlreadyOpen(m)


def _stop_cvmfs_transaction(repo, exc):
    m = 'Caught exception type {0}'.format(exc.__class__.__name__)
    exc_info = not isinstance(exc, ShutDown)
    log.fatal(m, exc_info=exc_info)
    log.fatal('Aborting CVMFS transaction')
    _abort_cvmfs_transaction(repo)


def _run_publish(repo):
    log.info('Closing CVMFS transaction')
    try:
        publish(repo)
    except (ShutDown, Exception, KeyboardInterrupt, SystemExit) as e:
        log.fatal('cvmfs publish raised an exception:', exc_info=True)
        log.fatal('Aborting CVMFS transaction')
        _abort_cvmfs_transaction(repo)


def _abort_cvmfs_transaction(repo):
    cmd = _abort_transaction_cmd(repo)
    result = shellCmd(cmd)
    return result


def _start_transaction_cmd(repo):
    return '{0} transaction {1}'.format(CVMFS_CMD, repo)


def _abort_transaction_cmd(repo):
    # -f option forces the abort with no y/n interactivity
    return '{0} abort -f {1}'.format(CVMFS_CMD, repo)


def _end_transaction_cmd(repo):
    return '{0} publish {1}'.format(CVMFS_CMD, repo)
