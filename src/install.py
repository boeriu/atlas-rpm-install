#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""install.py - install nightly RPMs to CVMFS.

    Note: To run this script, user must be the one defined in cfg.SUDO_USER.

- Usage:
    ./install.py -r <branch>/<platform>/<release> -n <nicosDatetime>

    where, for example:
    -r 22.0.X-VAL/x86_64-slc6-gcc49-opt/rel_3
    -n "2016-08-18T1234"

    Note that both of these are required.

- Brief description:
    The script execution logic is:
    1. Acquire lock
    2. Start a CVMFS transaction
    3. Install RPMs
    4. Run checks
    5. Publish CVMFS updates (closing the transaction)
    6. Release lock

- Detailed description:
    1. The lock is a simple text file created in the LOCK_DIR.
    It has a name like lock.<start>.<pid> where <start> is the
    integer epoch time when this install.py script was launched,
    and <pid> is the process ID of this install.py process.

    The acquire() method in the Lock() instance will check for
    existing locks in LOCK_DIR, of which there should be 0 or 1.
    If a lock is found from a different install process, create a
    lock request file in the LOCK_DIR with name like:
        lockrequest.<start>.<pid>
    where <start> and <pid> are as for lock files. If several install
    processes are launched, whilst a first one is doing its thing,
    there will be >1 lock request files. When the first install process
    is done, and deletes its lock file, the other install.py processes,
    which are in a 'sleep/check lock' cycle, will eventually notice
    the lock has gone. Each install.py instance will then sort by
    <start> time the lock request files, but only the instance whose
    request file is first in the list i.e. the oldest, will proceed to
    acquire the lock (and delete its request file). The other instances
    will resume sleeping. Note that there is a configurable value
    LOCK_ACQUISITION_MAX_WAIT that defines the maximum number of seconds
    that an install process should wait to acquire the lock before exiting.
    Default is 30 minutes. If set to 0, try once only. If set to -1,
    wait indefinitely.

    The install process whose lock request file is first in
    queue will do more than simply note the presence of the lock file
    called lock.<start>.<pid>. It will actively check that the process
    still exists and is doing something useful. If the PID is missing,
    the install process will delete the old lock, and create its own.
    If the PID is present, but something is awry, it will send email
    to alert human operators, before going back to sleep.

    2. Once the lock is acquired, the admin command:
        cvmfs_server transaction <repository>
    is executed. This command will exit with exitcode 1 if a transaction
    is already ongoing, otherwise 0. If the former case is encountered,
    this install script will exit. This scenario should not be possible,
    and thus is to be taken seriously. If the current install script
    has successfully obtained the lock, it should imply that there are
    no open cvmfs transactions.

    3. Installing RPMs means downloading and configuring ayum, and then
    using it to install.

    4. After installation, checks must be run to ensure that all is well.

    5. If the checks pass, then publish the updates (closing the transaction).

    6. Finally, release the lock by deleting the lock file.
"""

import getpass
import sys
import cfg

if getpass.getuser() != cfg.SUDO_USER:
    message = 'Must sudo to user {0} to run this script'.format(cfg.SUDO_USER)
    sys.stderr.write(message + '\n')
    sys.stderr.flush()
    sys.exit(2)


from ayumsetup import Ayum
from cfg import CVMFS_REPO, INSTALL_BASE_DIR, WORK_BASE_DIR
import cli
import cvmfs
import exc
from lock import lock, LOCK_FILE
import log
import monitor
import os
import process
from repos import getRemoteRepos, nightlyRepoURL, listNightlyRPMs
import signal
import shutil
import time
from util import sendEmail, createDir

# --------------------------------------------------------------
# Signal handling
# If kill -15 (SIGTERM) is sent to this install process
# the shutdown() function is executed
# --------------------------------------------------------------
def shutdown(signum, frame):
    """Executed on kill -15 signal. Grabs the pid of this
    install process, and then walks through and terminates
    any child processes, send an email, and then exits.
    """
    topProcessID = process.THIS_PID
    log.fatal('kill -TERM signal is caught, will kill child processes')
    monitor.process.error('Install process killed')
    for proc in process.tree(topProcessID):
        m = 'kill -9 {0} ({1})'.format(proc.pid, proc.cmdline())
        log.fatal(m)
        if proc.is_running():
            proc.kill()

    sendShutDownEmail(topProcessID)
    # raising exception will cause clean shutdown from cvmfs/lock
    raise exc.ShutDown('Caught kill -15, shutting down cleanly')


def sendShutDownEmail(pid):
    now = time.ctime()
    args = cli.parse(validate=False)
    nightly = '/'.join([args.branch,
                        args.platform,
                        args.release,
                        args.datetime])

    logpath = log.constructPathToLogFile(args)
    subject = 'Nightly CVMFS install process was killed'
    body = """
    {0}:-
    The install PID {1} was shut down via kill -15 signal.

    The nightly:
       {2}
    was thus not installed on CVMFS.

    Full output available in the log file:
       {3}
    """.format(now, pid, nightly, logpath)
    sendEmail(subject, body, *cfg.EMAIL_ON_FAIL)


signal.signal(signal.SIGTERM, shutdown)

# --------------------------------------------------------------
# --------------------------------------------------------------
# --------------------------------------------------------------

def failIfAlreadyInstalled(nightlyInstallDir):
    """Prevent an attempt to install an existing nightly
       /cvmfs/<repo>/<branch>/<nicosDatetime>/
    """
    if os.path.exists(nightlyInstallDir):
        m = '{0}: already exists, will not overwrite'.format(nightlyInstallDir)
        log.fatal(m)
        monitor.process.error('Already installed')
        monitor.process.stop()
        sys.exit(1)


def splitRPMs(rpms):
    """If we are installing a full nightly release, we should
    split the install into two: the offline install first, and
    then the AtlasHLT install.
    """
    hlt = None
    offline = False
    rpmslist = []
    for rpm in rpms:
        if rpm.startswith('AtlasHLT'):
            hlt = rpm
        if rpm.startswith('AtlasOffline'):
            offline = True

    if hlt and offline:
        # bingo, let's do separate installs
        rpms.remove(hlt)
        rpmslist.append(rpms)
        rpmslist.append([hlt])
    else:
        rpmslist.append(rpms)

    return rpmslist


def main():
    args = cli.parse()
    log.init(args)
    logpath = log.constructPathToLogFile(args)

    monitor.init()
    monitor.process.log(logpath)
    monitor.process.start()

    # e.g. /cvmfs/<repo>/22.0.X-VAL/
    installDir = os.path.join(INSTALL_BASE_DIR, args.branch)

    # e.g. /cvmfs/<repo>/22.0.X-VAL/2016-08-23T2145
    nightlyInstallDir = os.path.join(installDir, args.datetime)
    monitor.process.installdir(nightlyInstallDir)

    # Stop if we already installed this
    failIfAlreadyInstalled(nightlyInstallDir)

    with lock(LOCK_FILE):
        # If two processes are launched back-to-back
        # installing the same nightly as each other, the previous
        # call to the fail function will not be triggered as at that
        # point in time, the first install will have not completed.
        # So let's just do another check now, once the lock is obtained.
        failIfAlreadyInstalled(nightlyInstallDir)

        with cvmfs.transaction(CVMFS_REPO):

            # Let's add another file handler to the root logger
            # This one will send output to a logfile containing
            # only ayum-configure/install related info. We write
            # this ayum log to the LOGS_DIR alongside the main install log.
            # Once ayum related activities are over, we will remove
            # this file handler, and copy the ayum log to CVMFS.
            ayumLog = log.constructAyumLogFileName(args)
            ayumLogPath = os.path.join(cfg.LOGS_DIR, ayumLog)
            log.addFileHandler(ayumLogPath)

            # ---------------------------------
            # -- download and configure ayum
            # ---------------------------------
            ayum = Ayum(WORK_BASE_DIR, installDir)
            ayum.download()
            ayum.preconfigure()
            ayum.configure()
            ayum.configureRemoteRepos(*getRemoteRepos(args, installDir))
            ayum.postconfigure()

            # ---------------------------------
            # -- ayum install the nightly
            # ---------------------------------
            # First, we get the list of all rpms to install
            url = nightlyRepoURL(args, sorted=True)
            rpms = listNightlyRPMs(url)

            # If this is a full nightly release, split into
            # two groups: offline and hlt
            branch = os.path.basename(installDir)
            isCacheNightly = branch.count('.') > 2
            rpmslist = [rpms] if isCacheNightly else splitRPMs(rpms)

            # Now we install each group of rpms (there will be
            # just one group for cache nightlies)
            installOK = []
            for rpms in rpmslist:
                try:
                    ayum.install(*rpms)
                    installOK.append(True)
                except exc.AyumInstallError:
                    installOK.append(False)
                    continue

            # Remove the last log handler which is the ayum file log handler.
            # Leaves just the main install log that outputs to cfg.LOGS_DIR
            log.removeLastHandler()

            if not any(installOK):
                m = 'No ayum installs succeeded'
                log.error(m)

            # Now let's copy the ayum log to the nightly install
            # directory on CVMFS. If it does not exist - because
            # ayum installs failed - then we create it ourselves
            srcFile = ayumLogPath
            dstDir = nightlyInstallDir
            if not createDir(dstDir):
                m = ('Unable to create the nightly install dir: {0}\n'
                     'so cannot copy the ayum install log to it.\n'
                     'A copy of this log is available at:\n{1}')
                m = m.format(dstDir, srcFile)
                log.error(m)
            else:
                shutil.copy2(srcFile, dstDir)
                log.info('ayum log copied to {0}'.format(dstDir))

            # ---------------------------------
            # -- create a CVMFS nested catalog
            # ---------------------------------
            cvmfs.createNestedCatalogs(installDir, nightlyInstallDir)

            # ---------------------------------
            # -- run checks to ensure install ok
            # ---------------------------------
            # N.B. commented out as not sure what needs to be done
            # but if we wish to be rigorous, we should implement this
            # runInstallChecks()

    monitor.process.stop()


if __name__ == '__main__':
    main()
