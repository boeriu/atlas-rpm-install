import os

SUDO_USER = 'cvatlasnightlies'
CVMFS_REPO = 'atlas-nightlies.cern.ch'

INSTALL_BASE_DIR = '/cvmfs/{0}/repo/sw'.format(CVMFS_REPO)
WORK_BASE_DIR = os.environ.get('HOME')
LOGS_DIR = os.path.join(WORK_BASE_DIR, 'logs')

# directory to store information useful for external monitoring
MONITOR_DIR = os.path.join(WORK_BASE_DIR, 'install.monitor')

# --------------------------------------------------------------------
# -- Remote repo URLs used by repos.py module ------------------------
# --------------------------------------------------------------------
RPM_REPO_BASE = 'http://cern.ch/atlas-software-dist-eos/RPMs/'
RPM_REPO_NIGHTLY_BASE = os.path.join(RPM_REPO_BASE, 'nightlies')
RPM_REPO_DATA = os.path.join(RPM_REPO_BASE, 'data')
RPM_REPO_LCG = 'http://cern.ch/service-spi/external/rpms/lcg'

RPM_TDAQ_BASE = 'http://cern.ch/atlas-tdaq-sw/yum'
RPM_REPO_TDAQ_NIGHTLY = os.path.join(RPM_TDAQ_BASE, 'tdaq/nightly')
RPM_REPO_TDAQ_TESTING = os.path.join(RPM_TDAQ_BASE, 'tdaq/testing')
RPM_REPO_DQM_COMMON_TESTING = os.path.join(RPM_TDAQ_BASE, 'dqm-common/testing')
RPM_REPO_TDAQ_COMMON_TESTING = os.path.join(RPM_TDAQ_BASE, 'tdaq-common/testing')

# --------------------------------------------------------------------

AYUM_SRC = 'https://gitlab.cern.ch/rhauser/ayum.git'
CVMFS_STABLE_RELEASES_REPO = "/cvmfs/atlas.cern.ch/repo/sw/software"

# When things go wrong, who you gonna call?
EMAIL_ON_FAIL = [
    'atlas.release@cern.ch'
]

# How many seconds should we wait to acquire the install lock
# (because some other install process is currently running),
# before giving up and exiting? If set to -1, wait indefinitely.
# If set to 0 or some low number, try only once.
LOCK_ACQUISITION_MAX_WAIT = 60 * 60

# How many seconds should we sleep in between two checks
# to see if we can acquire the lock and start installing?
SECS_BETWEEN_LOCK_CHECKS = 30

# The character used to separate the fields in tag file entries
TAGS_FILE_FIELD_SEPARATOR = ';'
