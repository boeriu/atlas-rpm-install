import os

from bs4 import BeautifulSoup
from cfg import RPM_REPO_NIGHTLY_BASE
from cfg import RPM_REPO_DATA
from cfg import RPM_REPO_LCG
from cfg import RPM_REPO_TDAQ_NIGHTLY
from cfg import RPM_REPO_TDAQ_TESTING
from cfg import RPM_REPO_DQM_COMMON_TESTING
from cfg import RPM_REPO_TDAQ_COMMON_TESTING
from exc import RPMListRetrievalFailure
import log
import monitor
from util import getURL

import requests


def listNightlyRPMs(url, timeout=60):
    """Look up the nightly RPM names listed at the index page of
    the given URL. Return the list of these names, or empty
    list if there is a problem. Time out if the response takes too long.
    """
    log.info('Retrieving nightly RPM list from:\n{0}'.format(url))
    try:
        httpCode, response = getURL(url, timeout=timeout)
    except requests.exceptions.Timeout:
        m = 'Timed out contacting RPM repo:\n{0}'.format(url)
        log.fatal(m)
        monitor.process.error(m)
        raise RPMListRetrievalFailure(m)
    else:
        if httpCode != 200:
            if httpCode == 404:
                m = 'Inexistant RPM repo: \n{0}'.format(url)
            else:
                m = 'HTTP {0} when contacting: \n{1}'.format(httpCode, url)

            monitor.process.error(m)
            raise RPMListRetrievalFailure(m)

    return parseRPMsPage(response.text)


def parseRPMsPage(page):
    """Parse the page of the nightly RPM repo, finding
    all links to .rpm files. Return the list found.
    """
    links = BeautifulSoup(page, 'html.parser').find_all('a')
    links = [l.get('href') for l in links if l.get('href').endswith('.rpm')]
    return links


def nightlyRepoURL(args, sorted=False):
    url = os.path.join(RPM_REPO_NIGHTLY_BASE,
                       args.branch,
                       args.platform,
                       args.release)
    # sort contents by date/time
    if sorted:
        url = '{0}/?C=M;O=A'.format(url)
    return url


def getRemoteRepos(args, installdir):
    return [
        Repo(label='atlas-offline-data',
             name='ATLAS offline data packages',
             baseurl=RPM_REPO_DATA),

        Repo(label='lcg',
             name='LCG Repository',
             baseurl=RPM_REPO_LCG,
             prefix=os.path.join(installdir, 'sw/lcg/releases')),

        Repo(label='tdaq-nightly',
             name='Nightly snapshots of TDAQ releases',
             baseurl=RPM_REPO_TDAQ_NIGHTLY),

        Repo(label='tdaq-testing',
             name='Non-official updates and patches for TDAQ releases',
             baseurl=RPM_REPO_TDAQ_TESTING),

        Repo(label='dqm-common-testing',
             name='dqm-common projects',
             baseurl=RPM_REPO_DQM_COMMON_TESTING),

        Repo(label='tdaq-common-testing',
             name='Non-official updates and patches for TDAQ releases',
             baseurl=RPM_REPO_TDAQ_COMMON_TESTING),

        Repo(label='atlas-offline-nightly',
             name='ATLAS offline nightly releases',
             baseurl=nightlyRepoURL(args),
             prefix=os.path.join(installdir, args.datetime))
    ]


class Repo:
    def __init__(self, label, name, baseurl, prefix=None, enabled=1):
        self.label = label
        self.name = name
        self.baseurl = baseurl
        self.prefix = prefix
        self.enabled = enabled

    def __str__(self):
        repo = []
        repo.append('[{0}]'.format(self.label))
        repo.append('name={0}'.format(self.name))
        repo.append('baseurl={0}'.format(self.baseurl))
        repo.append('enabled={0}'.format(self.enabled))
        if self.prefix:
            repo.append('prefix={0}'.format(self.prefix))
        return '\n'.join(repo) + '\n'
