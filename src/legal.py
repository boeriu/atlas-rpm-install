# Legal values for platform components (used by cli.py)
BINARY = ['i686', 'x86_64']
OS = ['slc6', 'centos7']
COMPILER= ['gcc47', 'gcc48', 'gcc49', 'gcc62']
BUILD = ['opt', 'dbg']
