import cfg
from collections import namedtuple
from exc import AyumDownloadError
from exc import AyumConfigureError
from exc import AyumInstallError
import installerrors
import log
import monitor
import os
import re
import shutil
import sys
from util import shellCmd, timeit, createFile


class Ayum:
    def __init__(self, ayumdir, installdir):
        self.ayumdir = ayumdir
        self.installdir = installdir  # up to and including branch
        self.ayumBinary = os.path.join(self.ayumdir, 'ayum/ayum')

    def download(self):
        cmds = [
            'cd {0}'.format(self.ayumdir),
            'rm -rf ayum',
            'git clone {0}'.format(cfg.AYUM_SRC)
        ]
        result = shellCmd(';'.join(cmds))
        for o in result.out:
            log.info(o)

        if not result.ok:
            m = 'Problem downloading ayum\n'
            m += '(exitcode {0} for cmd: {1}'.format(result.exitcode,
                                                     result.cmd)

            monitor.process.error('ayum download error')
            log.fatal(m)
            raise AyumDownloadError(m)

    def preconfigure(self):
        """If this is a cache nightly, copy the RPM database directory
        from the stable release /cvmfs/atlas.cern.ch/ repo, so that
        dependencies can be found.
        """
        nightlyBranch = os.path.basename(self.installdir)
        isCacheNightly = (nightlyBranch.count('.') > 2)
        if not isCacheNightly:
            return

        toks = nightlyBranch.split('.')
        baseRel = '.'.join(toks[:2])  # e.g. 21.0
        stableRelSrc = os.path.join(cfg.CVMFS_STABLE_RELEASES_REPO, baseRel)
        if not os.path.exists(stableRelSrc):
            m = '{0}: main stable release does not exist, exiting'
            m = m.format(stableRelSrc)
            log.fatal(m)
            sys.exit(1)

        src = os.path.join(stableRelSrc, '.rpmdb')
        tgt = os.path.join(self.installdir, '.rpmdb')
        if os.path.exists(tgt):
            shutil.rmtree(tgt)

        log.debug('Copying {0} dir to {1}'.format(src, tgt))
        shutil.copytree(src, tgt)

    def configure(self):
        """Configure ayum for the nightly to be installed.
        Raises an AyumConfigureError if the configure command
        returns a non-zero exit code.
        """
        cmd = [
            'cd {0}/ayum'.format(self.ayumdir),
            './configure.ayum -i {0} -D > yum.conf'.format(self.installdir),
            self._fixYumConfCmd()
        ]

        cmd = ';'.join(cmd)
        result = shellCmd(cmd)
        for o in result.out:
            log.info(o)

        if not result.ok:
            m = 'Problem configuring ayum\n'
            m += '(exitcode {0} for cmd: {1}'.format(result.exitcode,
                                                     result.cmd)
            log.fatal(m)
            monitor.process.error('ayum config error')
            raise AyumConfigureError(m)

    def configureRemoteRepos(self, *repos):
        for repo in repos:
            path = os.path.join(self.ayumdir,
                                'ayum/etc/yum.repos.d',
                                '{0}.repo'.format(repo.label))
            if not createFile(path, str(repo), overwrite=True):
                log.warning('{0}: unable to write repo file'.format(path))

    def postconfigure(self):
        """Run an ayum clean all on the atlas-offline-nightly repo."""
        cleanAllCmd = '{0} --enablerepo=atlas-offline-nightly clean all'
        cleanAllCmd = cleanAllCmd.format(self.ayumBinary)
        cmds = [
            self._init_cmds(),
            cleanAllCmd
        ]
        result = shellCmd(';'.join(cmds))

        if not result.ok:
            m = 'Problem running ayum clean all command '
            m += '(exit code: {0})'.format(result.exitcode)
            log.warning(m)
            for e in result.err:
                log.warning(e)

    def install(self, *rpms):
        """Install the provided RPMs. Firstly, establish if any
        are already installed, and if so run an ayum reinstall.
        Otherwise just plain install. If install or reinstall exits
        with non-zero exitcode, raise AyumInstallError. This will
        abort the CVMFS transaction.
        """
        if not rpms:
            return None

        # remove the .rpm suffix leaving just the name
        rpms = [os.path.splitext(r)[0] for r in rpms]
        monitor.install.start(rpms)

        rpmsToInstall, rpmsToReinstall = self._categoriseByInstallStatus(rpms)

        totalInstallTime = 0
        for ri, rpms in ((True, rpmsToReinstall), (False, rpmsToInstall)):
            if not rpms:
                continue

            result = self._do_install(rpms, reinstall=ri)
            totalInstallTime += result.runtime
            if not self.installOK(result):
                reason = self.failureReason(result)
                monitor.install.failed(reason)
                monitor.timings.install(totalInstallTime)
                raise AyumInstallError('ayum (re)install failed')

        monitor.install.succeeded()
        monitor.timings.install(totalInstallTime)

    def installedPackages(self):
        """Return the list of already installed packages."""
        packages = []
        packageRegex = re.compile('(.*?).noarch\s+(.*?)')
        Package = namedtuple('Package', 'name version')
        for line in self._listInstalled():
            try:
                name, version = packageRegex.match(line).groups()
            except AttributeError:
                continue
            else:
                packages.append(Package(name, version))

        return packages

    def _fixYumConfCmd(self):
        cmd = [
            'sed "s/AYUM package location.*//" yum.conf > yum.conf.fixed',
            'mv yum.conf.fixed yum.conf'
        ]
        return ';'.join(cmd)

    def _init_cmds(self):
        return ';'.join([
            'cd {0}'.format(self.ayumdir),
            'shopt -s expand_aliases',
            'source ayum/setup.sh'
        ])

    def _listInstalled(self):
        cmds = [
            self._init_cmds(),
            '{0} -q list installed'.format(self.ayumBinary)
        ]

        result = shellCmd(';'.join(cmds))
        if not result.ok:
            noLocalPackages = (result.out == result.err)
            if noLocalPackages:
                log.info('No local packages installed')
            else:
                log.error('Cannot retrieve list of locally installed packages')
                log.error('Received exitcode {0}'.format(result.exitcode))
                for e in result.err:
                    log.error(e)
            return []

        return result.out

    def _categoriseByInstallStatus(self, rpms):
        """Split an input list of RPM names into two: one list
        containing RPMS already locally installed, the other list for
        RPMS not yet installed.
        """
        installed = self.installedPackages()
        installedNames = [package.name for package in installed]
        do_reinstall = []
        do_install = []
        for rpm in rpms:
            if rpm in installedNames:
                do_reinstall.append(rpm)
            else:
                do_install.append(rpm)
        return (do_install, do_reinstall)

    def _do_install(self, rpms, reinstall=False):
        rpms = ' '.join(rpms)
        action = 'reinstall' if reinstall else 'install'
        cmds = [
            self._init_cmds(),
            '{0} -y {1} {2}'.format(self.ayumBinary, action, rpms)
        ]
        cmd = ';'.join(cmds)
        cmd = ['bash', '-i', '-c', cmd]
        result = shellCmd(cmd, shell=False)
        return result

    def installOK(self, result, reinstall=False):
        """Examine the result object returned by shellCmd() to
        determine if there is an obvious problem with the install.
        If so, raise an AyumInstallError exception that will be caught
        by the CVMFS context manager and will cause a CVMFS transaction
        abort i.e. anything installed will be removed.
        """
        for line in result.out:
            log.info(line)

        action = 'Reinstall' if reinstall else 'Install'
        if result.ok:
            log.info('{0} looks to have been successful'.format(action))
        else:
            ec = result.exitcode
            log.error('{0} failed with exitcode {1}'.format(action, ec))
            for line in result.err:
                log.error(line)

        return result.ok

    def failureReason(self, result):
        """Try to figure out what went wrong, and return the reason
        as a string or empty string if we cannot figure it out.
        New failure reasons can be added to installerrors.py.
        """
        errors = installerrors.get()

        for line in result.err:
            for error in errors:
                if error.match(line):
                    if not error.matchall:
                        return str(error)
                    else:
                        # we found a match, go to the next line
                        break

        for error in knownInstallErrors:
            if error.matches:
                return str(error)

        return ''
