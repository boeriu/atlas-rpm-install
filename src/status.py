#! /usr/bin/env python

"""status.py - report on the status of installations.

Usage:

    python status.py --[format] [-n days]

where:
    [format] is one of --html | --text | --json
    [-n days] is the number of days to go back in time (if absent, default: 7)
"""

import argparse
from collections import namedtuple, defaultdict
import datetime
import json
import operator
import os
import re
import time
import sys

import cfg

FILE_NAME_REGEX= (
    '(.*?)__'  # branch
    '(.*?)__'  # platform
    '(rel_\d)__'  # release
    '(\d{4}-\d\d-\d\dT\d\d\d\d)__'  # nicos build start date/time
    '(\d{10})'  # epoch of install process start time
)
FILE_NAME_TOKENS = ('branch', 'platform', 'release', 'buildStart', 'start')

NOW = datetime.datetime.now()
NICOS_BUILD_DT_1 = re.compile('(\d{4})-(\d{2})-(\d{2})T(\d{2})(\d{2})')
NICOS_BUILD_DT_2 = re.compile('(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})')

# --------------------------------------------------------------------
# - Main actors ------------------------------------------------------
# --------------------------------------------------------------------

def memoize(func):
    cache = {}
    def wrapped(a):
        if a not in cache:
            cache[a] = func(a)
        return cache[a]
    return wrapped


class Run(object):
    def __init__(self, runInfo, jsonfile):
        self.__dict__.update(runInfo)
        self._convertNICOSBuildStart()
        self.parse(jsonfile)
        self.nightly = os.path.join(self.branch, self.platform)
        self.daysOld = ageInNicosDays(self)
        self.organisation = 'VO-atlas'
        self.type = 'nightly'

    def parse(self, jsonfile):
        # Refer to monitor.py for a full description
        # of this JSON data structure
        info = readJSONFile(jsonfile)
        self._parseProcessInfo(info['process'])
        self._parseInstallInfo(info['install'])
        self._parseTimingsInfo(info['timings'])

    def _constructInstallBaseDir(self):
        return os.path.join(cfg.INSTALL_BASE_DIR, self.branch, self.buildStart)

    def _constructLogFilePath(self):
        logname = '__'.join([
            self.branch,
            self.platform,
            self.release,
            self.buildStart,
            str(self.start)
        ])
        logname += '.log'
        return os.path.join(cfg.LOGS_DIR, logname)

    def _parseProcessInfo(self, process):
        """Parse the process monitoring info. The process info relates
        to global state of the install process.
        """
        # The path to the install log and the install directory on CVMFS
        # are now also written to the monitoring json file. During the
        # transition period of 1 week until all json files of the previous
        # week have this information, we use the temporary solution of
        # constructing the paths if the monitoring file does not contain them.
        # The 'or' part of these two statements should be removed after this
        # code has been 1 week in production.
        self.installLog = process.get('log') or self._constructLogFilePath()
        self.installDir = process.get('installdir') or self._constructInstallBaseDir()

        self.stop = process.get('stop')
        self.done = (self.stop is not None)

        states = process['states']
        if not states:
            # the process has not yet reached the lock acquire attempt
            # otherwise there would be either state waiting or running
            # appended to the states list
            self.processState = 'init'
            self.processStateInfo = ''
        else:
            lastState = states[-1]
            self.processState = lastState['name'].lower()
            self.processStateInfo = lastState['info']

    def _parseInstallInfo(self, installs):
        """Parse the install monitoring info. This relates to
        the actual install attempt(s), what RPMS were installed,
        outcome, etc. Each item in the @installs list corresponds to an
        'ayum (re)install' attempt, and is a dict of the form:
        {'start': integer_epoch,
         'stop': integer_epoch,
         'state': 'ok',
         'rpms':[list of rpms installed]
        }
        """
        # just need to bind it
        self.installs = installs

    def _parseTimingsInfo(self, timings):
        """Parse the timings info for the install attempts.
        Is a dict of the form:
        {'install':[..integer seconds to install for each attempt],
        'publish': integer seconds to publish everything
        }
        """
        self.timings = timings

    def _convertNICOSBuildStart(self):
        """Convert the build start datetime string from
        YYYY-MM-DDTHHMM to a more readable YYYY-MM-DD HH:MM.
        """
        bs = self.buildStart
        bs = bs.replace('T', ' ')
        bs = bs[:-2] + ':' + bs[-2:]
        self.buildStartHuman = bs

    def hasErrorState(self):
        """Was there an error at the process level, that prevented
        the installation from occuring? e.g. another CVMFS transaction
        is ongoing
        """
        return self.processState == 'err'

    def hasUnknownState(self):
        return self.processState == ''

    def tagsEntry(self):
        """Construct the line that should be entered into the
        nightly tags file.
        """
        separator = cfg.TAGS_FILE_FIELD_SEPARATOR
        return separator.join([
            self.organisation,
            self.type,
            self.branch,
            self.buildStart,
            self.platform
        ])


# --------------------------------------------------------------------
# - Formatters -------------------------------------------------------
# --------------------------------------------------------------------

class Text(object):
    def __init__(self, runs, ndays):
        self.runs = runs
        self.ndays = ndays

    def __str__(self):
        # TO DO
        return '--text option not yet operational, try HTML instead'


class HTML(object):
    def __init__(self, runs, ndays):
        self.runs = runs
        self.ndays = ndays

    def __str__(self):
        runs = self.groupRuns()
        runs = self.filterRuns(runs)

        page = HTMLPage('Status: ATLAS CVMFS Nightly Installs')
        page.head = self.pageHead()

        page.body += self.banner()
        page.body += self.lastUpdateTime()

        table = Table()
        table += self.tableHeaderRow()

        for nightly in sorted(runs):
            runsInNightly = runs[nightly]

            # Add the main summary row for this nightly over the last ndays
            table += self.tableSummaryRow(nightly, runsInNightly)

            for daysAgo in loopOverDays(self.ndays):
                run = runsInNightly[daysAgo]
                if run:
                    table += self.tableDetailedRow(nightly, run, daysAgo)

        page.body += table
        page.body += self.endOfBodyJS()
        return str(page)

    def filterRuns(self, runs):
        new = defaultdict(lambda: defaultdict(lambda: None))

        for nightly, runinfo in runs.items():
            for day, dayruns in runinfo.items():
                dayruns = sorted(dayruns,
                                 key=operator.attrgetter('start'),
                                 reverse=True)
                for run in dayruns:
                    if 'Already installed' in run.processStateInfo:
                        continue
                    new[nightly][day] = run
        return new

    def pageHead(self):
        head = HTMLHead()
        head.css = HTMLTag('style', self.css())
        head.scripts.append(HTMLTag('script', src='zepto.min.js'))
        return head

    def banner(self):
        return HTMLTag('div',
                       'Status of the CVMFS Nightly Installs',
                       id='banner')

    def lastUpdateTime(self):
        now = NOW.strftime('%Y-%m-%d %H:%M')
        return HTMLTag('div',
                       'Last Update: {0}'.format(now),
                        id='lastupdate')

    def css(self):
        return """
        html,body{margin:0;padding:0;border:0}
        body{
            font-family:Century Gothic,CenturyGothic,AppleGothic,sans-serif;
            background-color:#eee;
        }
        #banner{
            padding-left:10px;
            position:relative;
            top:0;
            left:0;
            background-color:black;
            color:white;
            font-size:1.5em;
            padding:10px;
        }
        #lastupdate{
            top:0;
            right:0;
            padding-right:10px;
            padding-top:5px;
            position:absolute;
            color:white;
            font-style:italic;
        }

        /* -- rel_X boxes ----------------------------- */
        .active{border:2px black solid}
        .box{
            padding-left:15px;
            width:80px;
            height:30px;
            border-radius:5px;
        }
        .empty{background-color:#ddd;opacity:0.5;}

        /* -- States ---------------------------------- */
        .fail{background-color:#CD5C5C;}
        .ok{background-color:#8FBC8F;}
        .err{background-color:#ee3;}
        .run{background-color:white;}
        .pend{background-color:white;}
        .unkwn{background-color:#bbb;}

        /* -- Header Cells ---------------------------- */
        .nightly{
            font-size:1.1em;
            border-right:1px black dotted;
            white-space:nowrap;
            padding-left:10px;
            padding-right:10px;
        }
        .legend td{
            border-bottom:1px black dotted;
        }

        .legend td:first-child{
            font-style:italic;
            background-color:#eee;
        }
        .legend td:not(:first-child) {
            font-size:1.1em;
            padding:10px;
            text-align:center;
            width:80px;
            height:20px;
        }

        /* -- Table and hidden info sub tables -------- */
        tr[id$='_tgt']{
            display:none;
        }
        .subtable {border:1px black dotted;width:100%;background-color:#eee;border-radius:5px}
        .subtable td{font-size:0.8em;padding:5px;vertical-align:top}
        .subheader td{
            font-size:1em;
            background-color:#ccc;
            border-radius:5px;
            padding:10px;
        }
        .subheader td a{color:black}
        .subheader_path{font-size:0.7em;}
        .subsubheader{font-size:1em;background-color:#ddd;}
        .subsubheader td{border-radius:5px;}
        .indent{padding-left:50px;}

        /* -- Animation on page load ------------------ */
        @keyframes highlight{
             from{background-color:#eee;}
             to{background-color:yellow;}
        }
        #help{
            animation-name:highlight;
            animation-duration:0.5s;
            animation-iteration-count: 2;
            animation-direction: alternate;
        }
        """

    def endOfBodyJS(self):
        return HTMLTag('script', """
        $(".box:not(.empty)").click(function(){
           var tgt = $(this).attr("id") + "_tgt";
           $("#" + tgt).toggle();
           $(this).hasClass('active')? $(this).removeClass('active'): $(this).addClass('active');
        });
        """)


    def tableSummaryRow(self, nightly, runsByDay):
        """Generate the HTML table row for a given branch/platform
        combination. The row is comprised of a first cell containing
        the nightly name and then one cell for each day.
        """
        row = TableRow()
        row.cell(nightly, klass='nightly')

        childRows = []
        for daysAgo in loopOverDays(self.ndays):
            run = runsByDay.get(daysAgo)
            if not run:
                row.cell(klass="empty box")
            else:
                state = 'err' if run.hasErrorState() else self.getSummaryState(run)
                nightlyID = self.constructNightlyID(nightly)
                parentID = 'src_{0}_{1}_{2}'.format(nightlyID,
                                                    run.release,
                                                    daysAgo)

                content = '{0}: {1}'.format(run.release, state.upper())
                row.cell(content, id=parentID, klass='box {0}'.format(state))

        return row

    def getSummaryState(self, run):
        """Get the overall state of the install attempts.
        If any installs have state fail, overall state is fail.
        """
        outcomes = [install['state'] for install in run.installs]
        if 'fail' in outcomes:
            state = 'fail'
        elif 'run' in outcomes:
            state = 'run'
        elif 'ok' in outcomes:
            state = 'ok'
        else:
            state = 'unkwn'
        return state

    def constructNightlyID(self, nightly):
        """Construct the HTML tag ID from this nightly string."""
        nightlyID = nightly.replace('/', '_')
        nightlyID = nightlyID.replace('.', '_')
        nightlyID = nightlyID.replace('-', '_')
        return nightlyID

    def tableDetailedRow(self, nightly, run, daysAgo):
        # Construct some HTML element IDs for some javascript fun
        nightlyID = self.constructNightlyID(nightly)
        parentID = 'src_{0}_{1}_{2}'.format(nightlyID,
                                            run.release,
                                            daysAgo)
        childID = '{0}_tgt'.format(parentID)

        detailedRow = TableRow(id=childID)

        cell = TableCell(colspan=str(self.ndays + 1), klass='indent')
        cell += self._createSubTable(run)
        detailedRow += cell
        return detailedRow

    def _createSubTableHeaderRow(self, run):
        nicosURL = constructNICOSURL(run.branch, run.platform, run.release)
        repoURL = cfg.RPM_REPO_NIGHTLY_BASE
        repoURL = os.path.join(repoURL, run.branch, run.platform, run.release)
        content = [run.release]
        if run.daysOld < 7:
            content.extend([
                str(Link('NICOS', href=nicosURL)),
                str(Link('Repo', href=repoURL))
            ])
        content = ' &mdash; '.join(content)
        content += str(HTMLTag('div',
                               'Install Dir: {0}'.format(run.installDir),
                                klass='subheader_path'))
        content += str(HTMLTag('div',
                               'Install Log: {0}'.format(run.installLog),
                                klass='subheader_path'))
        headerRow = TableRow(klass='subheader')
        headerRow.cell(content, colspan=str(self.ndays + 1))
        return headerRow

    def _createSubTableErrorRow(self, info):
        row = TableRow()
        row.cell(info)
        return row

    def _createSubTable(self, run):
        # Construct some URLs to resources for this nightly
        infotable = Table(klass='subtable')
        infotable += self._createSubTableHeaderRow(run)

        if run.hasErrorState():
            infotable += self._createSubTableErrorRow(run.processStateInfo)
        else:
            infotable += self._createSubTableInstallRows(run)

        return infotable

    def _createSubTableInstallRows(self, run):
        installRows = []

        row = TableRow(klass='subsubheader')
        row.cell('Status')
        row.cell('Project')
        row.cell('NICOS Build')
        row.cell('Install Start')
        row.cell('Install Done')
        row.cell('Info')
        row.cell('Timings', colspan='2')
        installRows.append(row)

        missingDeps = re.compile('Missing deps\s+\((.*?)\)', re.DOTALL)
        for index, install in enumerate(run.installs):
            row = TableRow()
            row.cell(install['state'].upper(), klass=install['state'])
            projectHeirarchy = install['rpms'][-1].split('_')[0]
            row.cell(projectHeirarchy)
            row.cell(run.buildStartHuman)
            row.cell(epochToStr(install['start']))
            row.cell(epochToStr(install['stop']) if install['stop'] else 'n/a')

            # Let's get the info about this install
            # It will be inexistant if the install was ok, or is running
            # If it failed however, we will get the reason why
            # If no info, give the install time instead.
            info = install.get('info')
            if not info:
                row.cell('n/a')
            else:
                match = missingDeps.match(info)
                if match:
                    deps = sorted(set(match.groups()[0].split()))
                    info = 'Missing Dependencies:<br>{0}'
                    info = info.format('<br>'.join(deps))

                row.cell(info, style='width:40%')

            # Install time, only relevant if install ok
            if install['state'] == 'ok':
                installTime = toMinsSecs(run.timings['install'][index])
                row.cell('Install time: {0}'.format(installTime))
            else:
                row.cell('n/a')

            # Only add this for the first row as it spans all rows
            if index == 0:
                publishTime = run.timings['publish']
                content = 'n/a'
                if publishTime:
                    content = toMinsSecs(publishTime)
                    content = 'Publish time: {0}'.format(content)
                row.cell(content, rowspan=str(len(run.installs)))

            # OK, done configuring our row
            installRows.append(row)

        return installRows

    def tableHeaderRow(self):
        row = TableRow(klass='legend')
        row.cell('(Click a rel_X box for more info)', id='help')

        for daysAgo in loopOverDays(self.ndays):
            lookup = {0: 'Today', 1: 'Yesterday'}
            text = lookup.get(daysAgo, '{0} days'.format(daysAgo))
            row.cell(text)

        return row

    def groupRuns(self):
        """Group the runs by nightly (primary key), then by
        days ago (secondary key).
        """
        grouped = defaultdict(lambda: defaultdict(list))
        for r in self.runs:
            grouped[r.nightly][r.daysOld].append(r)
        return grouped


class JSON(object):
    def __init__(self, runs, ndays):
        self.runs = runs
        self.ndays = ndays

    def __str__(self):
        # TO DO
        return '--json option not yet operational, try HTML instead'

# --------------------------------------------------------------------
# - HTML Helper classes ----------------------------------------------
# --------------------------------------------------------------------

class HTMLTag(object):
    def __init__(self, name, content='', **attrs):
        self.name = name
        self.value = content
        self.attrs = TagAttrs(**attrs)

    def __str__(self):
        return '<{0}{1}>{2}</{0}>'.format(self.name, self.attrs, self.value)


class Link(object):
    def __init__(self, name, **attrs):
        self.name = name
        self.attrs = attrs

    def __str__(self):
        return str(HTMLTag('a', self.name, **self.attrs))


class TagAttrs(object):
    def __init__(self, **attrs):
        self.attrs = attrs

    def __str__(self):
        tagattrs = []
        for n, v in self.attrs.items():
            # could not pass in class as it's
            # a reserved python keyword
            if n == 'klass':
                n = 'class'
            tagattrs.append('{0}="{1}"'.format(n, v))
        tagattrs = ' '.join(tagattrs)
        tagattrs = ' ' + tagattrs if tagattrs else ''
        return tagattrs


class HTMLPage(object):
    def __init__(self, title=''):
        self.decl = '<!doctype html>'
        self.head = HTMLHead()
        self.body = HTMLBody()

    def __str__(self):
        o = self.decl + '\n'
        o += str(HTMLTag('html', str(self.head) + str(self.body)))
        return o


class HTMLBody(object):
    def __init__(self):
        self.content = []

    def __iadd__(self, tag):
        self.content.append(tag)
        return self

    def __str__(self):
        content = '\n'.join([str(s) for s in self.content])
        return str(HTMLTag('body', content))


class HTMLHead(object):
    def __init__(self):
        self.css = None
        self.scripts = []

    def __str__(self):
        css = str(self.css)
        js = '\n'.join([str(s) for s in self.scripts])
        return str(HTMLTag('head', css + js))


class Table(object):
    def __init__(self, **attrs):
        self.rows = []
        self.attrs = attrs

    def row(self, **attrs):
        self.rows.append(TableRow(**attrs))

    def __iadd__(self, rows):
        if type(rows) in (list, tuple):
            self.rows.extend(rows)
        else:
            self.rows.append(rows)
        return self

    def __str__(self):
        rows = '\n'.join([str(tr) for tr in self.rows])
        return str(HTMLTag('table', rows, **self.attrs))


class TableRow(object):
    def __init__(self, **attrs):
        self.cells = []
        self.attrs = attrs

    def cell(self, content='', **attrs):
        self.cells.append(TableCell(content, **attrs))

    def __iadd__(self, cells):
        if type(cells) in (list, tuple):
            self.cells.extend(cells)
        else:
            self.cells.append(cells)
        return self

    def __str__(self):
        cells = '\n'.join([str(td) for td in self.cells])
        return str(HTMLTag('tr', cells, **self.attrs))


class TableCell(object):
    def __init__(self, content='', **attrs):
        self.content = content
        self.attrs = attrs

    def __iadd__(self, element):
        # Add an element to this cell
        self.content = str(element)
        return self

    def __str__(self):
        return str(HTMLTag('td', self.content, **self.attrs))

# --------------------------------------------------------------------
# - Helper functions -------------------------------------------------
# --------------------------------------------------------------------

def eosDownload(src, tgt):
    pass

def afsDownload(src, tgt):
    pass


def downloadLog(logfile):
    shutil.copyfile()


def loopOverDays(ndays):
    for daysAgo in range(ndays):
        yield daysAgo


def constructNICOSURL(branch, platform, release):
    """Get the URL to this nightly on the NICOS nightlies web."""
    nicosURL = ('http://atlas-nightlies-browser.cern.ch/'
                '~platinum/nightlies/info?tp=g&'
                'nightly={0}&rel={1}&ar={2}')
    nicosURL = nicosURL.format(branch, release, platform)
    return nicosURL


def constructEOSRepoURL(branch, platform, release):
    repoURL = cfg.RPM_REPO_NIGHTLY_BASE
    repoURL = os.path.join(repoURL, branch, platform, release)
    return repoURL


def epochStartToday():
    """Get the epoch integer seconds corresponding to the start of
    the NICOS day (defined as 21:00)
    """
    startDayHour = 21  # nicos midnight is 9pm
    if NOW.hour >= startDayHour:
        midnight = startDayHour
        offset = 0
    else:
        midnight = 0
        offset = 24 - startDayHour

    return int(time.time()) - secondsSinceMidnight(midnight) - (offset * 3600)


def secondsSinceMidnight(midnightHour=0):
    diffHours = NOW.hour - midnightHour
    diffMins = NOW.minute
    diffSecs = NOW.second
    totdiff = (diffHours * 3600) + (diffMins * 60) + diffSecs
    return totdiff


def ageInNicosDays(run):
    """Get the number of NICOS days ago when this build was made available."""
    todayStart = epochStartToday()
    buildStart = nicosDatetimeToEpoch(run.buildStart, NICOS_BUILD_DT_1)

    daysAgo = 0
    if buildStart < todayStart:
        daysAgo = ((todayStart - buildStart) / (24 * 3600)) + 1
    return daysAgo


def nicosDatetimeToEpoch(dt, regex):
    """Convert the NICOS datetime string to an epoch integer seconds.
    Use the regex passed to do the matching.
    """
    tokens = regex.match(dt).groups()
    year, month, day, hours, mins = [int(i) for i in tokens]
    dt = datetime.datetime(year, month, day, hours, mins)
    return int(time.mktime(dt.timetuple()))


def removeDuplicates(runs):
    """Remove runs with identical NICOS build start datetimes,
    returning only the youngest run (according to install start time).
    Duplicates may arise if we try more than once to install
    a given release (perhaps the first time there's a server
    timeout, etc).
    """
    byRelease = defaultdict(list)
    for run in runs:
        byRelease[run.release].append((run.start, run))

    runs = []
    for release, installs in byRelease.items():
        for runstart, run in sorted(installs, reverse=True):
            info = run.status[1]
            # If we accidentally try and install the same nightly
            # again, the corresponding monitor dir will be the
            # most recent one and thus will be shown, when in fact
            # the more interesting/appropriate result to show is
            # the actual install attempt one
            if 'Already installed' in info:
                continue
            runs.append(run)
            break
    return runs


def matchFiles(entries, filenameRegex):
    for e in entries:
        match = filenameRegex.match(e)
        if match:
            yield (e, match)


def getFiles(fileRegex, fileDir, ndays=7):
    """Find all files matching the fileRegex in fileDir that are younger
    than ndays old. Return as a generator. If ndays is -1, return all.
    If ndays is 0, return today's files, where today means between the
    start of this NICOS day and now. NICOS start of day is defined as
    21:00 CERN time. Note that the ndays filter applies to the NICOS
    build sart, not the install process start time.
    """
    fetchAll = (ndays == -1)
    entries = os.listdir(fileDir)

    for entry, match in matchFiles(entries, fileRegex):
        runInfo = dict(zip(FILE_NAME_TOKENS, match.groups()))

        if not fetchAll:
            epochToday = epochStartToday()
            earliestEpoch = epochToday - (24 * 3600 * ndays)
            buildEpoch = nicosDatetimeToEpoch(runInfo['buildStart'],
                                              NICOS_BUILD_DT_1)
            tooOld = (buildEpoch < earliestEpoch)
            if tooOld:
                continue

        # Return the json file name dict, and
        yield (runInfo, os.path.join(filedir, entry))


def getLogFiles(ndays=7):
    regex = re.compile(FILE_NAME_REGEX + '.log')
    return getFiles(regex, cfg.LOGS_DIR, ndays)


def getJSONFiles(ndays=7):
    """Match all monitoring JSON files younger than ndays.
    Return a generator of a 2-tuple of (jsonfileNameDict, jsonfile) where:
    (1) jsonfileNameDict is a dict constructed from the components of
    the JSON file name, which is of the form:
    <branch>__<platform>__<release>__<nicosBuildStart>__<epoch>.json
    where <epoch> is the install process start time, and <nicosBuildStart>
    is the datetime string YYYY-MM-DDTHHMM. Note that the ndays filter
    applies to the NICOS build sart, not the install process start time.
    (2) jsonfile is the full path to the JSON monitoring file
    """
    regex = re.compile(FILE_NAME_REGEX + '.json')
    return getFiles(regex, cfg.MONITOR_DIR, ndays)


def readJSONFile(jsonfile):
    with open(jsonfile) as f:
        return json.load(f)


def epochToStr(epoch):
    try:
        lt = time.localtime(float(epoch))
    except:
        return str(epoch)
    else:
        return time.strftime('%Y-%m-%d %H:%M', lt)


def toMinsSecs(secs):
    mins, secs = divmod(int(secs), 60)
    secs = secs if secs >= 10 else ('0' + str(secs))
    return '{0}m{1}s'.format(mins, secs)


def installs(ndays=7, logfileTgt=None):
    """Get the install attempts in the last ndays, and if
    requested, download the associated install logs to the
    logfileTgt directory.
    """
    jsonfiles = getJSONFiles(ndays)
    runs = [Run(runInfo, jsonfile) for (runInfo, jsonfile) in jsonfiles]
    runs = [r for r in sorted(runs,
                              key=operator.attrgetter('start'),
                              reverse=True)]
    return runs


def getFormatter(fmt):
    return (fmt.text and Text) or (fmt.json and JSON) or (fmt.html and HTML)


def parseArgs():
    parser = argparse.ArgumentParser('Get the status of past/current installs')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--text', action='store_true', help='Output as text')
    group.add_argument('--html', action='store_true', help='Output as HTML')
    group.add_argument('--json', action='store_true', help='Output as JSON')

    helpText = ('Show status for all installs within the '
                'last N days (default: 7). '
                'Put 0 to get today')
    parser.add_argument('-n', dest='ndays', help=helpText, type=int, default=7)

    logText = 'Download logs to the given destination path.'
    parser.add_argument('-d', dest='logdest', help=logText)

    args = parser.parse_args()
    if args.ndays < 0:
        sys.stderr.write('-n option requires a positive integer\n')
        sys.exit(1)
    return args


def main():
    args = parseArgs()
    runs = installs(args.ndays)
    formatter = getFormatter(args)
    print(formatter(runs, args.ndays))


if __name__ == '__main__':
    main()
