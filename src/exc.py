"""Custom exceptions for easier debugging."""


class AyumDownloadError(Exception):
    pass


class AyumConfigureError(Exception):
    pass


class AyumInstallError(Exception):
    pass


class CVMFSTransactionAlreadyOpen(Exception):
    pass


class CVMFSPublishError(Exception):
    pass


class CVMFSAbortTransaction(Exception):
    pass


class LockAcquireFailure(Exception):
    pass


class LockAcquireTimeOut(Exception):
    pass


class RPMListRetrievalFailure(Exception):
    pass


class ShutDown(Exception):
    pass


class TooManyLocks(Exception):
    pass


class URLInexistant(Exception):
    pass


class URLRetrieveFail(Exception):
    pass
