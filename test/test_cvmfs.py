import cvmfs
import pytest
import mock

import cfg
from collections import namedtuple
from exc import CVMFSTransactionAlreadyOpen


def failedShellCall():
    return namedtuple('Fail', 'ok')('False')


def test_transaction_ongoing_raises():
    repo = cfg.CVMFS_REPO
    with pytest.raises(CVMFSTransactionAlreadyOpen):
        with mock.patch('util.shellCmd') as mock_shell:
            mock_shell.return_value = failedShellCall()
            cvmfs._start_cvmfs_transaction(repo)
