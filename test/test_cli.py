import pytest
import sys
import cli


def test_parseargs_missing_release_raises():
    sys.argv[1:] = ['-n', '2016-08-23T1234']
    with pytest.raises(SystemExit):
        cli.parseArgs()


def test_parseargs_extra_arg_raises():
    sys.argv[1:] = ['-r',
                    '22.0.X/x86_64-slc6-gcc49-opt/rel_4',
                    '-n',
                    '2016-08-23T1234',
                    '-e',
                    'extra_arg']
    with pytest.raises(SystemExit):
        cli.parseArgs()


def test_parseargs_missing_datetime_raises():
    sys.argv[1:] = ['-r', '22.0.X/x86_64-slc6-gcc49-opt/rel_4']
    with pytest.raises(SystemExit):
        cli.parseArgs()


def test_parseargs_missing_args_raises():
    with pytest.raises(SystemExit):
        cli.parseArgs()


def test_parse_with_no_validate():
    sys.argv[1:] = ['-r',
                    '22.0.X/x86_64-slc6-gcc49-opt/rel_4',
                    '-n',
                    '2016-08-23T1234']
    args = cli.parse(validate=False)
    assert isinstance(args, tuple)
    assert args.branch == '22.0.X'
    assert args.platform == 'x86_64-slc6-gcc49-opt'
    assert args.release == 'rel_4'
    assert args.datetime == '2016-08-23T1234'


def test_parse_with_validate():
    sys.argv[1:] = ['-r',
                    '22.0.X/x86_64-slc6-gcc49-opt/rel_4',
                    '-n',
                    '2016-08-23T1234']
    args = cli.parse(validate=True)
    assert isinstance(args, tuple)
    assert args.branch == '22.0.X'
    assert args.platform == 'x86_64-slc6-gcc49-opt'
    assert args.release == 'rel_4'
    assert args.datetime == '2016-08-23T1234'


def test_invalid_format_release_arg_raises():
    with pytest.raises(ValueError):
        cli._validateRelease('bad_release')


def test_release_bad_platform_raises():
    with pytest.raises(ValueError):
        cli._validateRelease('22.0.X/bad_platform/rel_3')


def test_release_bad_releasename_raises():
    with pytest.raises(ValueError):
        cli._validateRelease('22.0.X/x86_64-slc6-gcc49-opt/rel_34')


def test_invalid_datetime():
    year = 2016
    month = 2
    day = 31
    hour = 5
    mins = 5
    assert not cli._isValidDatetime(year, month, day, hour, mins)


def test_valid_datetime():
    year = 2016
    month = 2
    day = 28
    hour = 5
    mins = 5
    assert cli._isValidDatetime(year, month, day, hour, mins)


def test_invalid_platform_raises():
    with pytest.raises(ValueError):
        cli._validatePlatform('inexistant')


def test_valid_platform():
    assert cli._validatePlatform('x86_64-slc6-gcc49-opt')


def test_invalid_release_name_raises():
    with pytest.raises(ValueError):
        cli._validateReleaseName('rel_34')


def test_valid_release_name():
    assert cli._validateReleaseName('rel_3')


def test_valid_nicosstamp():
    dt = '2016-09-27T1234'
    assert cli._validateNicosStamp(dt)


def test_invalid_format_nicosstamp_raises():
    with pytest.raises(ValueError):
        cli._validateNicosStamp('2016-09-2712:34')


def test_invalid_date_nicosstamp_raises():
    with pytest.raises(ValueError):
        cli._validateNicosStamp('2016-09-35T1234')
