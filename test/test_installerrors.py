import pytest
import re
import installerrors


def test_error_creation():
    patt = '.*?'
    callback = (lambda x: str(x))
    error = installerrors.Error(patt, callback)
    assert error.signature == re.compile(patt)
    assert str(error) == callback([])


def test_error_match_lines():
    patt = '\s+(.*?), stranger'
    callback = (lambda x: str(x))
    error = installerrors.Error(patt, callback)
    line = ' hello there, stranger'
    error.match(line)
    assert len(error.matches) == 1
    assert error.matches[0][0] == 'hello there'
